package com.rencredit.jschool.boruak.taskmanager.endpoint.rest;

import com.rencredit.jschool.boruak.taskmanager.api.endpoint.rest.IProjectRestEndpoint;
import com.rencredit.jschool.boruak.taskmanager.dto.ProjectDTO;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.exception.unknown.UnknownUserException;
import com.rencredit.jschool.boruak.taskmanager.service.ProjectService;
import com.rencredit.jschool.boruak.taskmanager.util.UserUtil;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/project")
public class ProjectRestEndpoint implements IProjectRestEndpoint {

    @Autowired
    private ProjectService projectService;

    @Override
    @PostMapping
    @PreAuthorize("isAuthenticated")
    public void create(@RequestBody ProjectDTO project) throws EmptyProjectException, EmptyUserIdException, EmptyLoginException, DeniedAccessException, UnknownUserException {
        projectService.save(UserUtil.getUserId(), project);
    }

    @Override
    @Nullable
    @GetMapping("/${id}")
    @PreAuthorize("isAuthenticated")
    public ProjectDTO findOneByIdDTO(@PathVariable("id") String id) throws EmptyIdException, EmptyUserIdException, EmptyLoginException, DeniedAccessException, UnknownUserException {
        return projectService.findByIdDTO(UserUtil.getUserId(), id);
    }

    @Override
    @GetMapping("/exist/${id}")
    @PreAuthorize("isAuthenticated")
    public boolean existsById(@PathVariable("id") String id) throws DeniedAccessException, UnknownUserException, EmptyUserIdException, EmptyProjectIdException {
        return projectService.existsByUserIdAndProjectId(UserUtil.getUserId(), id);
    }

    @Override
    @DeleteMapping("/${id}")
    @PreAuthorize("isAuthenticated")
    public void deleteOneById(@PathVariable("id") String id) throws DeniedAccessException, UnknownUserException, EmptyUserIdException, EmptyProjectIdException {
        projectService.deleteByUserIdAndProjectId(UserUtil.getUserId(), id);
    }

}
