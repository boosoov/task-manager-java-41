package com.rencredit.jschool.boruak.taskmanager.endpoint.rest;

import com.rencredit.jschool.boruak.taskmanager.api.endpoint.rest.ITasksRestEndpoint;
import com.rencredit.jschool.boruak.taskmanager.dto.ProjectDTO;
import com.rencredit.jschool.boruak.taskmanager.dto.TaskDTO;
import com.rencredit.jschool.boruak.taskmanager.exception.denied.DeniedAccessException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyTaskListException;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.EmptyUserIdException;
import com.rencredit.jschool.boruak.taskmanager.exception.unknown.UnknownUserException;
import com.rencredit.jschool.boruak.taskmanager.service.TaskService;
import com.rencredit.jschool.boruak.taskmanager.util.UserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/tasks")
public class TasksRestEndpoint implements ITasksRestEndpoint {

    @Autowired
    private TaskService taskService;

    @Override
    @GetMapping
    @PreAuthorize("isAuthenticated")
    public List<TaskDTO> getListDTO() throws EmptyUserIdException, DeniedAccessException, UnknownUserException {
        return taskService.findAllByUserIdDTO(UserUtil.getUserId());
    }

    @Override
    @GetMapping("/all")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public List<TaskDTO> getListDTOAll() {
        return taskService.findListDTO();
    }

    @Override
    @PostMapping
    @PreAuthorize("isAuthenticated")
    public List<TaskDTO> saveAll(@RequestBody List<TaskDTO> list) throws DeniedAccessException, UnknownUserException, EmptyTaskListException {
        for (TaskDTO task : list) {
            task.setUserId(UserUtil.getUserId());
        }
        return taskService.saveAll(list);
    }

    @Override
    @GetMapping("/count")
    @PreAuthorize("isAuthenticated")
    public long count() throws DeniedAccessException, UnknownUserException {
        return taskService.countAllByUserId(UserUtil.getUserId());
    }

    @Override
    @DeleteMapping("/all")
    @PreAuthorize("isAuthenticated")
    public void deleteAll() throws EmptyUserIdException, DeniedAccessException, UnknownUserException {
        taskService.deleteAllByUserId(UserUtil.getUserId());
    }

}
