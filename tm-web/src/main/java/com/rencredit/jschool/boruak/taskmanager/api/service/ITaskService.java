package com.rencredit.jschool.boruak.taskmanager.api.service;

import com.rencredit.jschool.boruak.taskmanager.dto.TaskDTO;
import com.rencredit.jschool.boruak.taskmanager.entity.Task;
import com.rencredit.jschool.boruak.taskmanager.exception.empty.*;
import com.rencredit.jschool.boruak.taskmanager.exception.incorrect.IncorrectIndexException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface ITaskService {

    void save(@Nullable final String userId, @Nullable final String name) throws EmptyNameException, EmptyUserIdException;

    void save(@Nullable final String userId, @Nullable final String name, @Nullable final String description) throws EmptyDescriptionException, EmptyNameException, EmptyUserIdException;

    void save(@Nullable final TaskDTO task) throws EmptyTaskException, EmptyUserIdException;

    List<TaskDTO> saveAll(@Nullable final Iterable<TaskDTO> iterable) throws EmptyTaskListException;


    @Nullable
    TaskDTO findOneByIdDTO(@Nullable final String userId, @Nullable final String id) throws EmptyIdException, EmptyUserIdException;

    @Nullable
    TaskDTO findOneByIndexDTO(@Nullable final String userId, @Nullable final Integer index) throws EmptyUserIdException, IncorrectIndexException;

    @Nullable
    TaskDTO findOneByNameDTO(@Nullable final String userId, @Nullable final String name) throws EmptyUserIdException, EmptyNameException;

    @NotNull
    List<TaskDTO> findAllByUserIdDTO(@Nullable final String userId) throws EmptyUserIdException;

    @NotNull
    List<Task> findAllByUserIdEntity(@NotNull final String userId) throws EmptyUserIdException;

    @NotNull
    List<TaskDTO> findAllByProjectIdDTO(@Nullable final String projectId) throws EmptyProjectIdException;

    @NotNull
    List<Task> findListEntity();

    @NotNull List<TaskDTO> findListDTO();


    void attachTaskToProject(
            @Nullable final String userId,
            @Nullable final String taskId,
            @Nullable final String projectId
    ) throws EmptyTaskException, EmptyIdException, EmptyUserIdException, EmptyTaskIdException, EmptyProjectIdException;

    void detachTaskToProject(
            @Nullable final String userId,
            @Nullable final String taskId,
            @Nullable final String projectId
    ) throws EmptyTaskException, EmptyIdException, EmptyUserIdException, EmptyTaskIdException, EmptyProjectIdException;


    boolean existsById(@Nullable final String s) throws EmptyTaskIdException;

    boolean existsByUserIdAndTaskId(@NotNull final String userId, @NotNull final String taskId) throws EmptyUserIdException, EmptyTaskIdException;


    long count();

    long countAllByUserId(@NotNull final String userId);


    @Nullable
    Task deleteById(@NotNull final String userId, @NotNull final String taskId) throws EmptyUserIdException, EmptyIdException;

    @Nullable
    void deleteByIndex(@Nullable final String userId, @Nullable final Integer index) throws IncorrectIndexException, EmptyUserIdException, EmptyTaskException;

    @Nullable
    void deleteByName(@Nullable final String userId, @Nullable final String name) throws EmptyNameException, EmptyUserIdException;

    void deleteList(@Nullable final Iterable<Task> iterable) throws EmptyTaskListException;

    void deleteAllByUserId(@Nullable final String userId) throws EmptyUserIdException;

    void deleteAll();

}
