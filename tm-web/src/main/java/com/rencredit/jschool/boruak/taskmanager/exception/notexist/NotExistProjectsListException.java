package com.rencredit.jschool.boruak.taskmanager.exception.notexist;

import com.rencredit.jschool.boruak.taskmanager.exception.AbstractException;

public class NotExistProjectsListException extends AbstractException {

    public NotExistProjectsListException() {
        super("Error! Project list is not exist...");
    }

}
