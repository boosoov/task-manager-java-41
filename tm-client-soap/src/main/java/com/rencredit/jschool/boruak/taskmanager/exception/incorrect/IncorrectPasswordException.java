package com.rencredit.jschool.boruak.taskmanager.exception.incorrect;

import com.rencredit.jschool.boruak.taskmanager.exception.AbstractClientException;

public class IncorrectPasswordException extends AbstractClientException {

    public IncorrectPasswordException() {
        super("Error! Password incorrect...");
    }

}
