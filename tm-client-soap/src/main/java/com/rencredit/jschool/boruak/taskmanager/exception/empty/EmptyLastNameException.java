package com.rencredit.jschool.boruak.taskmanager.exception.empty;

import com.rencredit.jschool.boruak.taskmanager.exception.AbstractClientException;

public class EmptyLastNameException extends AbstractClientException {

    public EmptyLastNameException() {
        super("Error! Last name is empty...");
    }

}
