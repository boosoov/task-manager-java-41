package com.rencredit.jschool.boruak.taskmanager.service;

import org.springframework.stereotype.Service;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Service
public class SessionService {

    private List<String> session;

    public List<String> getSession() {
        return session;
    }

    public void setSession(List<String> session) {
        this.session = session;
    }

    public static final String HEADER_COOKIE = "Cookie";

    public static final String HEADER_SET_COOKIE = "Set-Cookie";

    public static void setMaintain(final Object port) {
        final BindingProvider bindingProvider = (BindingProvider) port;
        final Map<String, Object> map = bindingProvider.getRequestContext();
        map.put(BindingProvider.SESSION_MAINTAIN_PROPERTY, true);
    }

    public static List<String> getListCookieRow(Object port) {
        final Map<String, Object> httpResponseHeaders = getHttpResponseHeaders(port);
        if (httpResponseHeaders == null) return null;
        return (List<String>) httpResponseHeaders.get(HEADER_SET_COOKIE);
    }

    public static Map<String, Object> getHttpResponseHeaders(Object port) {
        final Map<String, Object> responseContext = getResponseContext(port);
        if (responseContext == null) return null;
        return (Map<String, Object>) responseContext.get(MessageContext.HTTP_RESPONSE_HEADERS);
    }

    public static Map<String, Object> getResponseContext(Object port) {
        BindingProvider bindingProvider = getBindingProvider(port);
        if (bindingProvider == null) return null;
        return bindingProvider.getResponseContext();
    }

    public static BindingProvider getBindingProvider(Object port) {
        if (port == null) return null;
        return (BindingProvider) port;
    }

    public static void setListCookieRowRequest(Object port, final List<String> value) {
        if (getRequestContext(port) != null && getHttpRequestHeaders(port) == null) {
            getRequestContext (port).put(MessageContext.HTTP_REQUEST_HEADERS, new LinkedHashMap<>());
        }
        final Map<String, Object> httpRequestHeaders = getHttpRequestHeaders(port);
        if (httpRequestHeaders == null) return;
        httpRequestHeaders.put(HEADER_COOKIE, value);
    }

    public static Map<String, Object> getHttpRequestHeaders(Object port) {
        final Map<String, Object> requestContext = getRequestContext(port);
        if (requestContext == null) return null;
        return (Map<String, Object>) requestContext.get(MessageContext.HTTP_REQUEST_HEADERS);
    }

    public static Map<String, Object> getRequestContext(Object port) {
        BindingProvider bindingProvider = getBindingProvider(port);
        if (bindingProvider == null) return null;
        return bindingProvider.getRequestContext();
    }

}
